Homework assignment no. 1, Clock
====================================

**Publication date:**  11.3.2024

**Submission deadline:** 25.3.2024

## CHANGELOG

11.3.2024: Fixed example in javadoc of `ClockFormats.spelled()`

12.3.2024: Switch to JDK `17` in order to align with seminar project

General information
-------------------
This section provides general information about the initial structure of the provided codebase.  

### Project Structure
The structure of the project provided as a base for your implementation should meet the following criteria.

1. Package ```cz.muni.fi.pb162.hw01``` contains classes and interfaces provided as a part of the assignment.
- **Do not modify or add any classes or subpackages into this package.**
2. Package  ```cz.muni.fi.pb162.hw01.impl``` should contain your implementation.
- **Anything outside this package will be ignored during evaluation.**


Additionally, the following applies for the initial contents of ``cz.muni.fi.pb162.hw01.impl``

1) The information in **javadoc has precedence over everything**
2) **Interfaces** must be implemented
3) **Interfaces** must keep predefined methods
4) Otherwise, you can modify the code (unless tests are affected) as you see fit
5) When in doubt, **ASK**

**Note:**  
*While a modification of the interface is not strictly prohibited, you don't want to end with [god object](https://en.wikipedia.org/wiki/God_object) implementations.    
On the other hand, you want to adhere to the [single responsibility principle](https://en.wikipedia.org/wiki/Single-responsibility_principle).  
A rule of thumb should be not to modify interfaces unless required by the assignment. Consider extending the interface as an alternative.*

### Names in This Document
Unless fully classified name is provided, all class names are relative to the package ```cz.muni.fi.pb162.hw01``` or ```cz.muni.fi.pb162.hw01.impl``` for classes implemented as a part of your solution.

### Compiling the Project
The project can be compiled and packaged in the same way you already know.

```bash
$ mvn clean install
```

The only difference is that unlike the seminar project, the checks for missing documentation and a style violation will produce an error this time.
You can disable this behavior temporarily when running this command.

```bash
$ mvn clean install -Dcheckstyle.skip
```

You can consult your seminar teacher to help you set the ```checkstyle.skip``` property in your IDE (or just google it).

### Submitting the Assignment
Follow instructions of your tutor because the procedure to submit your solution may differ based on your seminar group. However, there are two ways of submission in general:
* Fork the project, develop your code in a development branch, and finally ask for the merge.
* Submit ```target/homework01-2024-1.0-SNAPSHOT-sources.jar``` to the homework vault.

### Minimal Requirements for Acceptance
- Fulfilling all Java course standards (documentation, conventions, etc.)
- Proper code decomposition
  - Split your code into multiple classes
  - Organize your classes in packages
- Single responsibility
  - Class should ideally have a single purpose
- Extendability
  - There could be multiple ways how to format time in the future. 


Assignment Description
-------------
The goal of this homework is to implement a simple program capable of handling time without dates. You will implement a `Clock` with the ability to add and subtract time. Furthermore, you will also provide means to format the time in text representation.

### Clock Properties
The following properties apply to `Clock` for the purpose of this assignment

- Clock start as 00:00:00.000
- Time is defined as a number of hours/minutes/seconds/milliseconds added to the starting time. 
- The number of hours/minutes/seconds/milliseconds used to initialize the clock can be arbitrary and it continuously rolls over to higher/lower units.
- While `Clock` doesn't necessarily support initialization of days, it keeps track of them. 
- There are certain limitations imposed by arithmetics over `long` data type, all tests are guaranteed to always initialize a time which fits into `Long.MAX_VALUE` when expressed in milliseconds (__Note: this is also a hint__).

### Implementation Notes
You are required to finish the implementation of

- `Clocks` which provides [factory methods](https://en.wikipedia.org/wiki/Factory_method_pattern) for your `Clock` implementation
- `ClockFormats` which serves as **testing API** for your implementation of formatting logic. See the next section for more details.

Consult javadocs for more details. Creating a main method and experimenting with your implementation on top of provided tests is highly recommended.

#### Clock Formatting 
When implementing the formatting logic, keep extendability in mind. There can be multiple ways how to format the time in the future. Your solution should reflect that. You should define a generic contract and implement two specific approaches.  

The class `ClockFormats` and its methods serves only as a **testing API** for your solution. This means that the actual implementation of the formatting logic must not be done inside these methods. The rule of thumb is that these methods should be very simple and only about 1-2 lines of code. Remember what you have learned about the object-oriented decomposition so far.    
