package cz.muni.fi.pb162.hw01;

public enum TimeUnit {
    DAYS,
    HOURS,
    MINUTES,
    SECONDS,
    MILLIS
}
