package cz.muni.fi.pb162.hw01;

/**
 * Representation of a clock
 */
public interface Clock {

    /**
     * Creates a new clock with time of the other clock added to the time of this clock
     *
     * @param other the other clock
     * @return new clock
     */
    Clock plus(Clock other);

    /**
     * Creates a new clock with time of the other clock subtracted from the time of this clock
     *
     * @param other the other clock
     * @return new clock
     */
    Clock minus(Clock other);

    /**
     * Returns this clock's time as given time unit
     *
     * @param unit time unit to use
     * @return time as given unit (rounded down)
     */
    long getAs(TimeUnit unit);

}
