package cz.muni.fi.pb162.hw01.impl;

import cz.muni.fi.pb162.hw01.TimeUnit;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class ClockCreationTest extends AbstractClockTest {

    @Test
    public void shouldCopyClock() {
        var clock1 = Clocks.instance(1, 59, 59, 7_322_005);
        var clock2 = Clocks.instance(clock1);

        Assertions.assertThat(clock2).isNotSameAs(clock1);
        Assertions.assertThat(clock2.getAs(TimeUnit.MILLIS)).isEqualTo(clock1.getAs(TimeUnit.MILLIS));
    }

    @Test
    public void shouldCreateAtMidnight() {
        var clock = Clocks.instance();
        assertClock(clock, 0, 0, 0, 0, 0);
    }

    @Test
    public void shouldCreateAtMidnightExplicit() {
        var clock = Clocks.instance(0, 0, 0, 0);
        assertClock(clock, 0, 0, 0, 0, 0);
    }

    @Test
    public void shouldCreatePositiveHoursFirstDay() {
        var clock = Clocks.instance(23, 0, 0, 0);
        assertClock(clock, 0, 23, 1_380, 82_800, 82_800_000);
    }

    @Test
    public void shouldCreatePositiveMinutesFirstDay() {
        var clock = Clocks.instance(0, 59, 0, 0);
        assertClock(clock, 0, 0, 59, 3_540, 3_540_000);
    }


    @Test
    public void shouldCreatePositiveSecondsFirstDay() {
        var clock = Clocks.instance(0, 0, 59, 0);
        assertClock(clock, 0, 0, 0, 59, 59_000);
    }

    @Test
    public void shouldCreatePositiveMillisFirstDay() {
        var clock = Clocks.instance(0, 0, 0, 999);
        assertClock(clock, 0, 0, 0, 0, 999);
    }

    @Test
    public void shouldCreatePositiveMixFirstDay() {
        var clock = Clocks.instance(18, 54, 54, 995);
        assertClock(clock, 0, 18, 1_134, 68_094, 68_094_995);
    }

    @Test
    public void shouldCreateNegativeHoursFirstDay() {
        var clock = Clocks.instance(-1, 0, 0, 0);
        assertClock(clock, 0, -1, -60, -3_600, -3_600_000);
    }

    @Test
    public void shouldCreateNegativeMinutesFirstDay() {
        var clock = Clocks.instance(0, -1, 0, 0);
        assertClock(clock, 0, 0, -1, -60, -60_000);
    }

    @Test
    public void shouldCreateNegativeSecondsFirstDay() {
        var clock = Clocks.instance(0, 0, -1, 0);
        assertClock(clock, 0, 0, 0, -1, -1_000);
    }

    @Test
    public void shouldCreateNegativeMillisFirstDay() {
        var clock = Clocks.instance(0, 0, 0, -1);
        assertClock(clock, 0, 0, 0, 0, -1);
    }

    @Test
    public void shouldCreateNegativeMixFirstDay() {
        var clock = Clocks.instance(-5, -5, -5, -5);
        assertClock(clock, 0, -5, -305, -18_305, -18_305_005);
    }

    @Test
    public void shouldRollOverPositiveMillisToSecondsExact() {
        var clock = Clocks.instance(0, 0, 0, 1_000);
        assertClock(clock, 0, 0, 0, 1, 1_000);
    }

    @Test
    public void shouldRollOverPositiveSecondsToMinutesExact() {
        var clock = Clocks.instance(0, 0, 60, 0);
        assertClock(clock, 0, 0, 1, 60, 60_000);
    }

    @Test
    public void shouldRollOverPositiveMinutesToHoursExact() {
        var clock = Clocks.instance(0, 60, 0, 0);
        assertClock(clock, 0, 1, 60, 3_600, 3_600_000);
    }

    @Test
    public void shouldRollOverPositiveMillisToSeconds() {
        var clock = Clocks.instance(0, 0, 0, 2_005);
        assertClock(clock, 0, 0, 0, 2, 2_005);
    }

    @Test
    public void shouldRollOverPositiveSecondsToMinutes() {
        var clock = Clocks.instance(0, 0, 125, 0);
        assertClock(clock, 0, 0, 2, 125, 125_000);
    }

    @Test
    public void shouldRollOverPositiveMinutesToHours() {
        var clock = Clocks.instance(0, 125, 0, 0);
        assertClock(clock, 0, 2, 125, 7_500, 7_500_000);
    }

    @Test
    public void shouldRollOverPositiveMillisToMinutes() {
        var clock = Clocks.instance(0, 0, 0, 122_005);
        assertClock(clock, 0, 0, 2, 122, 122_005);
    }

    @Test
    public void shouldRollOverPositiveSecondsToHours() {
        var clock = Clocks.instance(0, 0, 7325, 0);
        assertClock(clock, 0, 2, 122, 7325, 7_325_000);
    }

    @Test
    public void shouldRollOverPositiveMillisToHours() {
        var clock = Clocks.instance(0, 0, 0, 7_322_005);
        assertClock(clock, 0, 2, 122, 7_322, 7_322_005);
    }

    @Test
    public void shouldRollOverPositiveCascade() {
        var clock = Clocks.instance(1, 59, 59, 7_322_005);
        assertClock(clock, 0, 4, 242, 14_521, 14_521_005);
    }

    @Test
    public void shouldRollOverPositiveHoursExact() {
        var clock = Clocks.instance(24, 0, 0, 0);
        assertClock(clock, 1, 24, 1_440, 86_400, 86_400_000);
    }

    @Test
    public void shouldRollOverPositiveHours() {
        var clock = Clocks.instance(100, 0, 0, 0);
        assertClock(clock, 4, 100, 6_000, 360_000, 360_000_000);
    }

    @Test
    public void shouldRollOverNegativeMillisToSecondsExact() {
        var clock = Clocks.instance(0, 0, 0, -1_000);
        assertClock(clock, 0, 0, 0, -1, -1_000);
    }

    @Test
    public void shouldRollOverNegativeSecondsToMinutesExact() {
        var clock = Clocks.instance(0, 0, -60, 0);
        assertClock(clock, 0, 0, -1, -60, -60_000);
    }

    @Test
    public void shouldRollOverNegativeMinutesToHoursExact() {
        var clock = Clocks.instance(0, -60, 0, 0);
        assertClock(clock, 0, -1, -60, -3_600, -3_600_000);
    }

    @Test
    public void shouldRollOverNegativeMillisToSeconds() {
        var clock = Clocks.instance(0, 0, 0, -2_005);
        assertClock(clock, 0, 0, 0, -2, -2_005);
    }

    @Test
    public void shouldRollOverNegativeSecondsToMinutes() {
        var clock = Clocks.instance(0, 0, -125, 0);
        assertClock(clock, 0, 0, -2, -125, -125_000);
    }

    @Test
    public void shouldRollOverNegativeMinutesToHours() {
        var clock = Clocks.instance(0, -125, 0, 0);
        assertClock(clock, 0, -2, -125, -7_500, -7_500_000);
    }

    @Test
    public void shouldRollOverNegativeMillisToMinutes() {
        var clock = Clocks.instance(0, 0, 0, -122_005);
        assertClock(clock, 0, 0, -2, -122, -122_005);
    }

    @Test
    public void shouldRollOverNegativeSecondsToHours() {
        var clock = Clocks.instance(0, 0, -7_325, 0);
        assertClock(clock, 0, -2, -122, -7_325, -7_325_000);
    }

    @Test
    public void shouldRollOverNegativeHoursToMillis() {
        var clock = Clocks.instance(0, 0, 0, 7_322_005);
        assertClock(clock, 0, 2, 122, 7_322, 7_322_005);
    }

    @Test
    public void shouldRollOverNegativeCascade() {
        var clock = Clocks.instance(5, -59, -59, -7_322_005);
        assertClock(clock, 0, 1, 117, 7_078, 7_078_995);
    }

    @Test
    public void shouldRollOverNegativeHoursExact() {
        var clock = Clocks.instance(-24, 0, 0, 0);
        assertClock(clock, -1, -24, -1_440, -86_400, -86_400_000);
    }

    @Test
    public void shouldRollOverNegativeHours() {
        var clock = Clocks.instance(-100, 0, 0, 0);
        assertClock(clock, -4, -100, -6_000, -360_000, -360_000_000);
    }

    @Test
    public void shouldRollOverMixedPositiveFirstDay() {
        var clock = Clocks.instance(12, 87, -65, 1205);
        assertClock(clock, 0, 13, 805, 48_356, 48_356_205);
    }

    @Test
    public void shouldRollOverMixedFirstDay() {
        var clock = Clocks.instance(1, -87, -65, 1205);
        assertClock(clock, 0, 0, -28, -1_683, -1_683_795);
    }

    @Test
    public void shouldRollOverMixed() {
        var clock = Clocks.instance(-23, -87, -65, 1205);
        assertClock(clock, -1, -24, -1_468, -88_083, -88083795);
    }
}
