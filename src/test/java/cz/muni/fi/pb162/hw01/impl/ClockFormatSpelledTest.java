package cz.muni.fi.pb162.hw01.impl;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class ClockFormatSpelledTest extends AbstractClockTest {

    @Test
    public void shouldFormatClockAllUnitAbove1() {
        var clock = Clocks.instance(4, 22, 42, 300);

        System.out.println(ClockFormats.spelled(clock));
        Assertions.assertThat(ClockFormats.spelled(clock)).isEqualTo("four hours, twenty-two minutes and forty-two seconds");
    }

    @Test
    public void shouldFormatClockAllUnitAt1() {
        var clock = Clocks.instance(1, 1, 1, 1);

        System.out.println(ClockFormats.spelled(clock));
        Assertions.assertThat(ClockFormats.spelled(clock)).isEqualTo("one hour, one minute and one second");
    }

    @Test
    public void shouldFormatClockMissingHours() {
        var clock = Clocks.instance(0, 22, 42, 300);

        System.out.println(ClockFormats.spelled(clock));
        Assertions.assertThat(ClockFormats.spelled(clock)).isEqualTo("twenty-two minutes and forty-two seconds");
    }

    @Test
    public void shouldFormatClockMissingMinutes() {
        var clock = Clocks.instance(4, 0, 42, 300);

        System.out.println(ClockFormats.spelled(clock));
        Assertions.assertThat(ClockFormats.spelled(clock)).isEqualTo("four hours and forty-two seconds");
    }

    @Test
    public void shouldFormatClockMissingSeconds() {
        var clock = Clocks.instance(4, 22, 0, 300);

        System.out.println(ClockFormats.spelled(clock));
        Assertions.assertThat(ClockFormats.spelled(clock)).isEqualTo("four hours and twenty-two minutes");
    }
}
