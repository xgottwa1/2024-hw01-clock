package cz.muni.fi.pb162.hw01.impl;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ClockOperationTest extends AbstractClockTest {

    @Test
    public void shouldAddTwoMidnights() {
        var clock1 = Clocks.instance();
        var clock2 = Clocks.instance();
        var result1 = clock1.plus(clock2);
        var result2 = clock2.plus(clock1);

        assertClock(result1, 0, 0, 0, 0, 0);
        assertClock(result2, 0, 0, 0, 0, 0);
    }

    @Test
    public void shouldReturnNewClockFromAdd() {
        var clock1 = Clocks.instance();
        var clock2 = Clocks.instance();
        var result1 = clock1.plus(clock2);

        assertThat(result1).isNotSameAs(clock1);
        assertThat(result1).isNotSameAs(clock2);
    }

    @Test
    public void shouldSubtractTwoMidnights() {
        var clock1 = Clocks.instance();
        var clock2 = Clocks.instance();
        var result1 = clock1.minus(clock2);
        var result2 = clock2.minus(clock1);

        assertClock(result1, 0, 0, 0, 0, 0);
        assertClock(result2, 0, 0, 0, 0, 0);
    }

    @Test
    public void shouldReturnNewClockFromMinus() {
        var clock1 = Clocks.instance();
        var clock2 = Clocks.instance();
        var result1 = clock1.minus(clock2);

        assertThat(result1).isNotSameAs(clock1);
        assertThat(result1).isNotSameAs(clock2);
    }

    @Test
    public void shouldAddMillis() {
        var clock1 = Clocks.instance(0,0,0,200);
        var clock2 = Clocks.instance(0,0,0,200);
        var result1 = clock1.plus(clock2);
        var result2 = clock2.plus(clock1);

        assertClock(result1, 0, 0, 0, 0, 400);
        assertClock(result2, 0, 0, 0, 0, 400);
    }

    @Test
    public void shouldAddSeconds() {
        var clock1 = Clocks.instance(0,0,20,200);
        var clock2 = Clocks.instance(0,0,20,200);
        var result1 = clock1.plus(clock2);
        var result2 = clock2.plus(clock1);

        assertClock(result1, 0, 0, 0, 40, 40_400);
        assertClock(result2, 0, 0, 0, 40, 40_400);
    }

    @Test
    public void shouldAddMinutes() {
        var clock1 = Clocks.instance(0,10,20,200);
        var clock2 = Clocks.instance(0,10,20,200);
        var result1 = clock1.plus(clock2);
        var result2 = clock2.plus(clock1);

        assertClock(result1, 0, 0, 20, 1_240, 1_240_400);
        assertClock(result2, 0, 0, 20, 1_240, 1_240_400);
    }

    @Test
    public void shouldAddHours() {
        var clock1 = Clocks.instance(5,10,20,200);
        var clock2 = Clocks.instance(5,10,20,200);
        var result1 = clock1.plus(clock2);
        var result2 = clock2.plus(clock1);

        assertClock(result1, 0, 10, 620, 37_240, 37_240_400);
        assertClock(result2, 0, 10, 620, 37_240, 37_240_400);
    }

    @Test
    public void shouldAddMillisRollOver() {
        var clock1 = Clocks.instance(0,0,0,800);
        var clock2 = Clocks.instance(0,0,0,300);
        var result1 = clock1.plus(clock2);
        var result2 = clock2.plus(clock1);

        assertClock(result1, 0, 0, 0, 1, 1100);
        assertClock(result2, 0, 0, 0, 1, 1100);
    }

    @Test
    public void shouldAddSecondsRollOver() {
        var clock1 = Clocks.instance(0,0,50,200);
        var clock2 = Clocks.instance(0,0,20,200);
        var result1 = clock1.plus(clock2);
        var result2 = clock2.plus(clock1);

        assertClock(result1, 0, 0, 1, 70, 70_400);
        assertClock(result2, 0, 0, 1, 70, 70_400);
    }

    @Test
    public void shouldAddMinutesRollOver() {
        var clock1 = Clocks.instance(0,50,20,200);
        var clock2 = Clocks.instance(0,20,20,200);
        var result1 = clock1.plus(clock2);
        var result2 = clock2.plus(clock1);

        assertClock(result1, 0, 1, 70, 4_240, 4_240_400);
        assertClock(result2, 0, 1, 70, 4_240, 4_240_400);
    }

    @Test
    public void shouldAddHoursRollOver() {
        var clock1 = Clocks.instance(13,10,20,200);
        var clock2 = Clocks.instance(12,10,20,200);
        var result1 = clock1.plus(clock2);
        var result2 = clock2.plus(clock1);

        assertClock(result1, 1, 25, 1_520, 91_240, 91_240_400);
        assertClock(result2, 1, 25, 1_520, 91_240, 91_240_400);
    }


    @Test
    public void shouldSubtractMillis() {
        var clock1 = Clocks.instance(0,0,0,200);
        var clock2 = Clocks.instance(0,0,0,100);
        var result1 = clock1.minus(clock2);

        assertClock(result1, 0, 0, 0, 0, 100);
    }

    @Test
    public void shouldSubtractSeconds() {
        var clock1 = Clocks.instance(0,0,20,200);
        var clock2 = Clocks.instance(0,0,10,100);
        var result1 = clock1.minus(clock2);

        assertClock(result1, 0, 0, 0, 10, 10_100);
    }

    @Test
    public void shouldSubtractMinutes() {
        var clock1 = Clocks.instance(0,10,20,200);
        var clock2 = Clocks.instance(0,5,10,100);
        var result1 = clock1.minus(clock2);

        assertClock(result1, 0, 0, 5, 310, 310_100);
    }

    @Test
    public void shouldSubtractHours() {
        var clock1 = Clocks.instance(5,10,20,200);
        var clock2 = Clocks.instance(2,5,10,100);
        var result1 = clock1.minus(clock2);

        assertClock(result1, 0, 3, 185, 11_110, 11_110_100);
    }

    @Test
    public void shouldSubtractMillisRollOver() {
        var clock1 = Clocks.instance(0,0,1,100);
        var clock2 = Clocks.instance(0,0,0,200);
        var result1 = clock1.minus(clock2);

        assertClock(result1, 0, 0, 0, 0, 900);
    }

    @Test
    public void shouldSubtractSecondsRollOver() {
        var clock1 = Clocks.instance(0,1,21,100);
        var clock2 = Clocks.instance(0,0,30,200);
        var result1 = clock1.minus(clock2);

        assertClock(result1, 0, 0, 0, 50, 50_900);
    }

    @Test
    public void shouldSubtractMinutesRollOver() {
        var clock1 = Clocks.instance(1,11,21,100);
        var clock2 = Clocks.instance(0,20,30,200);
        var result1 = clock1.minus(clock2);

        assertClock(result1, 0, 0, 50, 3_050, 3_050_900);
    }

    @Test
    public void shouldSubtractHoursRollOver() {
        var clock1 = Clocks.instance(1,11,21,100);
        var clock2 = Clocks.instance(2,20,30,200);
        var result1 = clock1.minus(clock2);

        assertClock(result1, 0, -1, -69, -4_149, -4_149_100);
    }

   @Test
   public void shouldAddWithOneRollOverPositive() {
        var clock1 = Clocks.instance(50,0,0,0);
        var clock2 = Clocks.instance(2,0,0,0);
        var result1 = clock1.plus(clock2);
        var result2 = clock2.plus(clock1);

        assertClock(result1, 2, 52, 3_120, 187_200, 187_200_000);
        assertClock(result2, 2, 52, 3_120, 187_200, 187_200_000);
    }

    @Test
    public void shouldAddWithOneRollOverPositiveToRollOver() {
        var clock1 = Clocks.instance(47,0,0,0);
        var clock2 = Clocks.instance(2,0,0,0);
        var result1 = clock1.plus(clock2);
        var result2 = clock2.plus(clock1);

        assertClock(result1, 2, 49, 2_940, 176_400, 176_400_000);
        assertClock(result2, 2, 49, 2_940, 176_400, 176_400_000);
    }

    @Test
    public void shouldAddWithOneRolloverNegative() {
        var clock1 = Clocks.instance(5,0,0,0);
        var clock2 = Clocks.instance(-30,0,0,0);
        var result1 = clock1.plus(clock2);
        var result2 = clock2.plus(clock1);

        assertClock(result1, -1, -25, -1_500, -90_000, -90_000_000);
        assertClock(result2, -1, -25, -1_500, -90_000, -90_000_000);
    }

    @Test
    public void shouldAddWithOneRolloverNegativeToRollover() {
        var clock1 = Clocks.instance(-20,0,0,0);
        var clock2 = Clocks.instance(-25,0,0,0);
        var result1 = clock1.plus(clock2);
        var result2 = clock2.plus(clock1);

        assertClock(result1, -1, -45, -2_700, -162_000, -162_000_000);
        assertClock(result2, -1, -45, -2_700, -162_000, -162_000_000);
    }

    @Test
    public void shouldAddWithBothRollOverPositive() {
        var clock1 = Clocks.instance(25,0,0,0);
        var clock2 = Clocks.instance(25,0,0,0);
        var result1 = clock1.plus(clock2);
        var result2 = clock2.plus(clock1);

        assertClock(result1, 2, 50, 3_000, 180_000, 180_000_000);
        assertClock(result2, 2, 50, 3_000, 180_000, 180_000_000);
    }

    @Test
    public void shouldAddWithBothRolloverNegative() {
        var clock1 = Clocks.instance(-30,0,0,0);
        var clock2 = Clocks.instance(-30,0,0,0);
        var result1 = clock1.plus(clock2);
        var result2 = clock2.plus(clock1);

        assertClock(result1, -2, -60, -3_600, -216_000, -216_000_000);
        assertClock(result2, -2, -60, -3_600, -216_000, -216_000_000);
    }

    @Test
    public void shouldAddWithBothRolloverPositiveNegativeExact() {
        var clock1 = Clocks.instance(30,0,0,0);
        var clock2 = Clocks.instance(-30,0,0,0);
        var result1 = clock1.plus(clock2);
        var result2 = clock2.plus(clock1);

        assertClock(result1, 0, 0, 0, 0, 0);
        assertClock(result2, 0, 0, 0, 0, 0);
    }

    @Test
    public void shouldSubtractWithOneRollOverPositive() {
        var clock1 = Clocks.instance(50,0,0,0);
        var clock2 = Clocks.instance(2,0,0,0);
        var result1 = clock1.minus(clock2);

        assertClock(result1, 2, 48, 2_880, 172_800, 172_800_000);
    }

    @Test
    public void shouldSubtractWithOneDayRollOverPositiveToDayRollOver() {
        var clock1 = Clocks.instance(47,0,0,0);
        var clock2 = Clocks.instance(25,0,0,0);
        var result1 = clock1.minus(clock2);

        assertClock(result1, 0, 22, 1_320, 79_200, 79_200_000);
    }

    @Test
    public void shouldSubtractWithRightDayRolloverNegative() {
        var clock1 = Clocks.instance(5,0,0,0);
        var clock2 = Clocks.instance(-30,0,0,0);
        var result1 = clock1.minus(clock2);

        assertClock(result1, 1, 35, 2_100, 126_000, 126_000_000);
    }

    @Test
    public void shouldSubtractWithLeftDayRolloverNegative() {
        var clock1 = Clocks.instance(-30,0,0,0);
        var clock2 = Clocks.instance(5,0,0,0);
        var result1 = clock1.minus(clock2);

        assertClock(result1, -1, -35, -2_100, -126_000, -126_000_000);
    }

    @Test
    public void shouldSubtractWithRightDayRolloverNegativeToDayRollover() {
        var clock1 = Clocks.instance(-20,0,0,0);
        var clock2 = Clocks.instance(-30,0,0,0);
        var result1 = clock1.minus(clock2);

        assertClock(result1, 0, 10, 600, 36_000, 36_000_000);
    }

    @Test
    public void shouldSubtractWithLeftDayRolloverNegativeToDayRollover() {
        var clock1 = Clocks.instance(-30,0,0,0);
        var clock2 = Clocks.instance(20,0,0,0);
        var result1 = clock1.minus(clock2);

        assertClock(result1, -2, -50, -3_000, -180_000, -180_000_000);
    }
}
